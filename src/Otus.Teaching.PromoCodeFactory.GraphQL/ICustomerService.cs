﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQl
{
    public interface ICustomerService
    {
        Task<ClientResponse> CreateCustomerAsync(CreateOrEditCustomerRequest request);

        Task<ClientResponse> DeleteCustomerAsync(Guid id);

        Task<ClientResponse> EditCustomerAsync(Guid id, CreateOrEditCustomerRequest request);

        Task<List<CustomerShortResponse>> GetCustomersAsync();

        Task<CustomerResponse> GetCustomerAsync(Guid id);
    }
}
