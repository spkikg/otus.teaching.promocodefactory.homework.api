﻿using System;

namespace Otus.Teaching.PromoCodeFactory.GraphQl
{
    public class ClientResponse
    {
        public Guid Id { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
