﻿using System;

namespace Otus.Teaching.PromoCodeFactory.GraphQl
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}