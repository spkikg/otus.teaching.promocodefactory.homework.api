﻿using HotChocolate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQl.GraphQl
{
    public class CustomerMutation
    {
        public async Task<ClientResponse> CreateCustomerAsync([Service] ICustomerService service, CreateOrEditCustomerRequest request)
        {
            return await service.CreateCustomerAsync(request);
        }

        public async Task<ClientResponse> DeleteCustomerAsync([Service] ICustomerService service, Guid id)
        {
            return await service.DeleteCustomerAsync(id);
        }

        public async Task<ClientResponse> EditCustomerAsync([Service] ICustomerService service, EditCustomerRequest request)
        {
            var id = request.Id;
            var innerRequest = new CreateOrEditCustomerRequest
            {
                FirstName = request.FirstName, 
                LastName = request.LastName, 
                Email = request.Email, 
                PreferenceIds = request.PreferenceIds
            };

            return await service.EditCustomerAsync(id, innerRequest);
        }
    }
}
