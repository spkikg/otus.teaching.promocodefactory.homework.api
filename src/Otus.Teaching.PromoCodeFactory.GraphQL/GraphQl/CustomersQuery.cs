﻿using HotChocolate;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQl.GraphQl
{
    public class CustomersQuery
    {
        [UsePaging]
        [UseFiltering]
        [UseSorting]
        public async Task<List<CustomerShortResponse>> GetCustomers([Service] ICustomerService service)
        {
            return await service.GetCustomersAsync();
        }

        [UsePaging]
        [UseFiltering]
        [UseSorting]
        public async Task<CustomerResponse[]> GetCustomer([Service] ICustomerService service, string id)
        {
            var guid = Guid.Parse(id);

            var response = await service.GetCustomerAsync(guid);

            return new[] { response };
        }
    }
}
